require_relative '../lib/cell'
require_relative '../lib/empty_cell'

describe Cell do

  let (:empty_cell){ EmptyCell.new }
  let (:x_cell){ XCell.new }
  let (:o_cell){ OCell.new }

  describe '#==' do

    context 'when two cells have' do

      before do
        @another_empty_cell = EmptyCell.new
      end

      it 'the same value theyre equal' do
      expect(empty_cell).to be == @another_empty_cell
      end

      it 'not the same value theyre not equal' do
        expect(x_cell).not_to be == o_cell
      end
    end
  end

  describe '#new' do

    context 'when creating an empty cell' do
      it 'has no content' do
        expect(empty_cell.value).to eq ''
      end
    end

  end

  describe '.play' do

    context 'when an  empty cell gets an  X' do

      it 'becomes an X cell' do
        expect(empty_cell.play_x.value).to eq 'X'
      end
    end

    context 'when an  empty cell gets an  O' do

      it 'becomes an O cell' do
        expect(empty_cell.play_o.value).to eq 'O'
      end
    end

    context 'when an O cell gets an X' do
      it 'raises an error' do
        expect{o_cell.play_x}.to raise_error(RuntimeError,'A cell cant change value')
      end
    end

    context 'when an X cell gets an O' do
      it 'raises an error' do
        expect{x_cell.play_o}.to raise_error(RuntimeError,'A cell cant change value')
      end
    end

    context 'when an X cell gets an X' do
      it 'raises an error' do
        expect{x_cell.play_x}.to raise_error(RuntimeError,'Youve already played an X here')
      end
    end

    context 'when an O cell gets an O' do
      it 'raises an error' do
        expect{o_cell.play_o}.to raise_error(RuntimeError,'Youve already played an O here')
      end
    end
  end
end