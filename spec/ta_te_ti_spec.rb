# rubocop:disable Layout/IndentationConsistency

require_relative '../lib/ta_te_ti'
require_relative '../lib/cell'
require_relative '../lib/empty_cell'


describe TaTeTi do

  let(:ta_te_ti) {TaTeTi.new}
  let(:empty_cell){EmptyCell.new}
  let(:x_cell) {XCell.new}

  describe '#new' do

    context 'when a game is created' do

      it 'the board has nine empty cells' do
        expect(ta_te_ti.board.all? do |row| row.all? {|cell| cell == empty_cell} end)
      end

    end
  end

  describe '.play' do

    context 'when an X gets played in a coordinate' do

      before do
        ta_te_ti.play_x 0, 1
      end

      it 'is placed in the board accordingly' do
        expect(ta_te_ti.board[0][1]).to be == x_cell
      end

    end

    context 'when there are three equal cells in a row' do

      before do
        ta_te_ti.play_x 0, 0
        ta_te_ti.play_x 0, 1
        ta_te_ti.play_x 0, 2
      end

      it 'the player wins the game' do
        expect(ta_te_ti.game_status).to eq ('Game won by X')
      end
    end

    context 'when there are three equal cells in a column' do

      before do
        ta_te_ti.play_x 0, 0
        ta_te_ti.play_x 1, 0
        ta_te_ti.play_x 2, 0
      end

      it 'the player wins the game' do
        expect(ta_te_ti.game_status).to eq ('Game won by X')
      end
    end

    context 'when there are three equal cells in one diagonal' do

      before do
        ta_te_ti.play_x 0, 0
        ta_te_ti.play_x 1, 1
        ta_te_ti.play_x 2, 2
        puts ta_te_ti.board
      end

      it 'the player wins the game' do
        expect(ta_te_ti.game_status).to eq ('Game won by X')
      end
    end

    context 'when there are three equal cells in the other diagonal' do

      before do
        ta_te_ti.play_x 0, 2
        ta_te_ti.play_x 1, 1
        ta_te_ti.play_x 2, 0
        puts ta_te_ti.board
      end

      it 'the player wins the game' do
        expect(ta_te_ti.game_status).to eq ('Game won by X')
      end
    end
  end
end
