require_relative '../lib/cell'
require_relative '../lib/empty_cell'

class TaTeTi

  def initialize
    @board = Array.new(3) {Array.new(3,EmptyCell.new)}
    @game_status = "No winner yet"
  end

  attr_accessor :board
  attr_accessor :game_status

  def play_x row, column
    @board[row][column] = @board[row][column].play_x
    last_played = @board[row][column]
    @game_status = has_winner(last_played) ? "Game won by #{last_played.value}" : "No winner yet"
  end

  def has_winner a_cell
    row_winner(a_cell) || column_winner(a_cell) || diagonal_one_winner(a_cell) || diagonal_two_winner(a_cell)
  end

  def row_winner a_cell
    won = false
    @board.each do |row|
      won = won || row.all? {|x| x == a_cell}
    end
    won
  end

  def column_winner a_cell
    won = false
    (0..2).each do |r|
      won = won || (0..2).all? {|c| @board[c][r] == a_cell }
    end
    puts won
    won
  end

  def diagonal_one_winner a_cell
    (0..2).all? {|pos| @board[pos][pos] == a_cell}
  end

  def diagonal_two_winner a_cell
    (0..2).all? {|pos| @board[0+pos][2-pos] == a_cell}
  end

end