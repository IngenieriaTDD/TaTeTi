class XCell < Cell

  def == another_cell
    self.value == another_cell.value
  end

  def hash
    value.hash
  end

  def initialize
    @value = 'X'
  end

  attr_accessor :value

  def play_x
   raise 'Youve already played an X here'
  end

  def play_o
    raise 'A cell cant change value'
  end

end