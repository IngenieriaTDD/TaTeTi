class OCell < Cell

  def == another_cell
    self.value == another_cell.value
  end

  def hash
    value.hash
  end

  def initialize
    @value = 'O'
  end

  attr_accessor :value

  def play_x
    raise 'A cell cant change value'
  end

  def play_o
    raise 'Youve already played an O here'
  end

end