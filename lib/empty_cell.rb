require_relative '../lib/x_cell'
require_relative '../lib/o_cell'

class EmptyCell < Cell

  def == another_cell
    self.value == another_cell.value
  end

  def hash
    value.hash
  end

  def initialize
    @value = ''
  end

  attr_accessor :value

  def play_x
    XCell.new
  end

  def play_o
    OCell.new
  end
end